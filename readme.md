# Running the server
# in server open terminal at project page

# prerequisites
 - python 3
 - pip install -r requirements.txt

# run these commands in server folder:
export FLASK_APP=hello.py
flask run --host=0.0.0.0
