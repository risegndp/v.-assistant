import snowboy_file_detector as snowboydecoder
import datetime
import time
from os import system
import sys

# keyword detected
def tathe_kiwen_a():
    print("tathe kiwen a?")

def time_ki_hoya():
    timestamp = time.time()
    value = datetime.datetime.fromtimestamp(timestamp)
    print('time ki hoya?')
    hour = int(value.strftime('%H'))
    minute = int(value.strftime('%M'))
    ampm = value.strftime('%p')
    if(hour > 12):
        hour = hour-12
    time_string = str(hour) + ' ' + str(minute) + ' ' + ampm
    print(time_string)
    # system('say ' + time_string)

def callHappy():
    print('calling happy')
    # system('say ok, calling happy')

def callDk():
    print('calling dk...')

def callGundeep():
    print('calling Gundeep..')

def chiragCallback():
    print('khare an')

# configure detector
# detector = snowboydecoder.HotwordDetector("tathe_kiwen_a.pmdl", sensitivity=0.5, audio_gain=1)
models = ["time_ki_hoya.pmdl", "happycall.pmdl", "calldk.pmdl", "gundeep.pmdl", "tka_pahadi.pmdl"]
detector2 = snowboydecoder.HotwordDetector(models, sensitivity=0.5, audio_gain=1, audio_file='file-testing.wav')

# start detection
# detector.start(tathe_kiwen_a)
model_callbacks = [time_ki_hoya, callHappy, callDk, callGundeep, chiragCallback]
output = detector2.start(model_callbacks)
f = open("result.txt", "w+")
f.write(str(output))
f.truncate()
f.close()
